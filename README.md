## gpdo

gpdo finds all the likely GOPATH candidates in your source directory tree and adds them to the 
GOPATH before executing whatever command it has been passed.

It will work in a project that is a single git project, and first works out where the git root
is located.  It then searches for directories that look like GOPATH members (directories having
a src dir with go files underneath).

### usage example 
```
gpdo do go build
```

### primary paths
If there are multiple go modules in the repository, we will nominate the path containing the
current path as a primary, putting it at the front of the GOPATH.

Any pre-existing generic GOPATH is post-pended to the end.

 <primary paths>:<secondary paths>:<existing GOPATH>

### Suggested usage 
```
alias gpdo="$PATH_TO_GPDO/gpdo"
gpdo do go build 
```

List modules in current repo:
```
gpdo list
```

Show what gpdo thinks the GOPATH should be:
```
gpdo show
```

Export the path (note the '.' to source gpdo in the current environment): 
```
. gpdo export
```

